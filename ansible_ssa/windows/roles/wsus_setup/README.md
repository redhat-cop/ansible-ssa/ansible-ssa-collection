# Ansible Role: wsus_setup

This role will help to deploy a Windows Server Update Services.

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml
- name: Deploy Windows Server Update Services
  var:
    wsus_content_dir: C:\WSUS
    wsus_client_targeting_mode: Client  #Client/Server
    wsus_category_names:
      CriticalUpdates:
        id: 'E6CF1350-C01B-414D-A61F-263D14D133B4'
        name: Critical Updates
      DefinitionUpdates:
        id: 'E0789628-CE08-4437-BE74-2495B842F43B'
        name: Definition Updates
      SecurityUpdates:
        id: '0FA1201D-4330-4FA8-8AE9-B877473B6441'
        name: Security Updates
      ServicePacks:
        id: '68C5B0A3-D1A6-4553-AE49-01D3A7827828'
        name: Service Packs
      UpdateRollups:
        id: '28BC880E-0592-4CBF-8F95-C79B17911D5F'
        name: Update Rollups Packs
      Updates:
        id: 'CD5FFD1E-E932-4E3A-BF74-18BF0B1BBD83'
        name: Updates
    wsus_products:
      - Windows Server 2008 R2
      - Windows Server 2012 R2
      - Windows Server 2016
      - Windows Server 2019
      - Microsoft Server operating system-21H2
      - Windows 10, version 1903 and later
  ansible.builtin.include_role:
    name: ansible_ssa.windows.wsus_setup
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
